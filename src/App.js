//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {TabHomeNavigator,HomeNavigator} from './Router';
// create a component
class App extends Component {
    render() {
        return (
            <HomeNavigator/>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the App
export default App;
