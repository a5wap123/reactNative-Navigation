//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native';

// create a component
class Detail extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>Tôi yêu {this.props.navigation.state.params.name}</Text>
                <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                    <Text>Back</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default Detail;
