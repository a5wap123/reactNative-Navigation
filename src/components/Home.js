//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet
,TouchableOpacity,Image } from 'react-native';

// create a component
class Home extends Component {
        
    render() {
        return (
            <View style={styles.container}>
                <Text>Home</Text>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('StackDetail',{name:'Khánh Hà'})}}>
                    <Text>Go Love</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    icon: {
      backgroundColor: 'green',
    width: 26,
    height: 26,
  },
});

//make this component available to the app
export default Home;
