import React, { Component } from 'react';
import {Image,StyleSheet,Platform} from 'react-native';
import {StackNavigator,TabNavigator} from 'react-navigation';
import Home from './components/Home';
import Detail from './components/Detail';
import User from './components/User';

const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
    marginBottom:0
  },
  label:{
      ...Platform.select({
          ios:{
              marginBottom:4,
              marginTop: -3
          },
          android:{
              marginBottom:-7,
              marginTop: -3
          }
      })
  }
});
export const TabHomeNavigator = TabNavigator({
    TabHome:{
        screen: Home,
        navigationOptions:{
            tabBarLabel:'Home',
            tabBarIcon: ({ tintColor }) => (
                <Image
                    source={require('./images/home.png')}
                    style={[styles.icon, {tintColor: '#fff'}]}
                />)
        },
    },
    TabUser:{
        screen:User,
        navigationOptions:{
            tabBarLabel:'User',
            tabBarIcon: ({ tintColor }) => (
                <Image
                    source={require('./images/user.png')}
                    style={[styles.icon, {tintColor: '#fff'}]}
                />)
        },
        
    },
},{
    tabBarPosition:'bottom',
    swipeEnabled:true,
    animationEnabled:true,
  tabBarOptions: {
    activeTintColor: '#fff',
    inactiveTintColor:'#fff',
    activeBackgroundColor:'#2980b9',
    upperCaseLabel:false,
    showIcon: true,
    indicatorStyle:{
        backgroundColor: '#fff'
    },
    labelStyle:styles.label,
    style:{
        backgroundColor: '#3498db',
    },
  },
})
export const HomeNavigator = StackNavigator({
    StackHome:{
        screen: TabHomeNavigator,
        navigationOptions:{
            title:'Home',
        }
    },
    StackDetail:{
        screen: Detail,
        navigationOptions:{
            title:'Detail'
        }
    },
},{
     headerMode: 'none',
})

